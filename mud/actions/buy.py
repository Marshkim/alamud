from .action import Action2, Action3
from mud.events import BuyEvent

class BuyAction(Action2):
    EVENT = BuyEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "buy"
