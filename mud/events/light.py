# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2
from .event import Event3

class LightOnEvent(Event2):
    NAME = "light-on"

    def perform(self):
        if not self.object.has_prop("lightable"):
            self.fail()
            return self.inform("light-on.failed")
        self.inform("light-on")

class LightOffEvent(Event2):
    NAME = "light-off"
    
    def perform(self):
	    if not self.object.has_prop("lightable") and not self.object.has_prop("inflammable"):
		    self.fail()
		    return self.inform("light-off.failed")
	    self.inform("light-off")


class LightWithEvent(Event3):
	NAME="light-with"
	
	def perform(self):
		if not self.object.has_prop("inflammable"):
			self.add_prop(object_not_inflammable)
		elif not self.object2.has_prop("igniteur"):
			self.add_prop(object_not_igniteur)
		else:
			return self.inform("light-with")
		self.fail()
		return self.inform("light-with.failed")
