from .event import *

class BuyEvent(Event2):
    NAME = "buy"
    
    def perform(self):
        if not self.object.has_prop("buyable"):
            self.add_prop("object-not-buyable")
            return self.take_failed()
        if self.object in self.actor:
            self.add_prop("object-already-in-inventory")
            return self.take_failed()
        self.object.move_to(self.actor)
        self.inform("buy")
